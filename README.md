# sodium-frp

Functional Reactive Programming (FRP) Library for multiple languages. https://github.com/SodiumFRP

* https://github.com/SodiumFRP/sodium

# Book
* [Functional reactive programming
  ](https://www.worldcat.org/search?q=Functional+reactive+programming)

# Main languages
* Java
* [sodium-rust](https://crates.io/crates/sodium-rust)
* Haskell: [reactive-banana](https://hackage.haskell.org/package/reactive-banana)